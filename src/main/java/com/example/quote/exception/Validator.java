package com.example.quote.exception;

import java.math.BigDecimal;

import static java.math.BigDecimal.ZERO;
import static java.math.BigDecimal.valueOf;
import static java.math.MathContext.UNLIMITED;

public final class Validator {

    private Validator() {
    }

    public static <T> T requireNotNull(T value) {
        if (value == null) {
            throw new IllegalArgumentException();
        }
        return value;
    }

    public static BigDecimal requirePositive(BigDecimal value) {
        if (requireNotNull(value).compareTo(ZERO) <= 0) {
            throw new IllegalArgumentException();
        }
        return value;
    }

    public static void validateInputParameters(String[] args) {
        if (args == null || args.length != 2 || args[0] == null || args[1] == null) {
            throw new InvalidInputParametersException("This program must be run with exactly 2 parameters");
        }
    }

    public static BigDecimal validateRequestedAmount(String s) {
        try {
            BigDecimal result = new BigDecimal(s);
            if (result.compareTo(valueOf(1000)) < 0) {
                throw new InvalidInputParametersException("requested amount must not be less than 1000");
            } else if (result.compareTo(valueOf(15000)) > 0) {
                throw new InvalidInputParametersException("requested amount must not be greater than 15000");
            } else if (result.remainder(valueOf(100), UNLIMITED).compareTo(ZERO) != 0) {
                throw new InvalidInputParametersException("requested amount must be divisible by 100");
            } else {
                return result;
            }
        } catch (NumberFormatException e) {
            throw new InvalidInputParametersException("requested amount must be a number: " + s);
        }
    }

}
