package com.example.quote.exception;

public class InvalidInputParametersException extends RuntimeException {

    public InvalidInputParametersException(String message) {
        super(message);
    }

}
