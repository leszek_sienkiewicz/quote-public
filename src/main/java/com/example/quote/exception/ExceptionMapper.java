package com.example.quote.exception;

public class ExceptionMapper {

    public String map(Exception e) {
        if (e instanceof CsvException) {
            return "There was a problem with the csv file: " + e.getMessage();
        } else if (e instanceof InsufficientOffersException) {
            return "There are not enough offers to provide a quote at this time.";
        } else if (e instanceof InvalidInputParametersException) {
            return "Input parameters are invalid: " + e.getMessage();
        } else {
            return "An unexpected error happened during processing your request.";
        }
    }

}
