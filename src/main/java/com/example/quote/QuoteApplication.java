package com.example.quote;

import com.example.quote.exception.ExceptionMapper;
import com.example.quote.lenderSource.CsvLenderSource;
import com.example.quote.lenderSource.LenderSource;
import com.example.quote.model.QuoteResult;
import com.example.quote.service.LoansBuilder;
import com.example.quote.service.QuoteCalculator;
import com.example.quote.service.RateCalculator;
import com.example.quote.service.RepaymentsCalculator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.example.quote.exception.Validator.validateRequestedAmount;
import static com.example.quote.exception.Validator.validateInputParameters;
import static java.util.Arrays.asList;

public class QuoteApplication {

    private static final Logger LOG = LoggerFactory.getLogger(QuoteApplication.class);

    public static void main(String[] args) {

        try {
            LOG.debug("starting QuoteApplication with arguments: " + asList(args));
            validateInputParameters(args);
            QuoteCalculator calculator = getQuoteCalculator(args);
            QuoteResult result = calculator.getQuote(validateRequestedAmount(args[1]));
            System.out.println(result);
        } catch (Exception e) {
            LOG.error("An exception was thrown while calculating quote: " + e, e);
            System.out.println(new ExceptionMapper().map(e));
        }
    }

    private static QuoteCalculator getQuoteCalculator(String[] args) {
        LenderSource lenderSource = new CsvLenderSource(args[0]);
        return new QuoteCalculator(lenderSource, new RateCalculator(), new LoansBuilder(), new RepaymentsCalculator());
    }

}
