package com.example.quote.service;

import java.math.BigDecimal;

import static com.example.quote.Constants.HIGH_PRECISION;
import static com.example.quote.Constants.MONTHS_IN_YEAR;
import static java.math.BigDecimal.ONE;

public class RepaymentsCalculator {

    /**
     * For details see https://en.wikipedia.org/wiki/Mortgage_calculator#Monthly_payment_formula
     */
    BigDecimal constantRepayment(BigDecimal principal, BigDecimal yearlyRate, int numberOfPayments) {
        BigDecimal monthlyRate = yearlyRate.divide(MONTHS_IN_YEAR, HIGH_PRECISION);
        BigDecimal onePlusMonthlyRateToThePowerOfNumberOfPayments = ONE.add(monthlyRate).pow(numberOfPayments, HIGH_PRECISION);

        BigDecimal nominator = monthlyRate.multiply(principal, HIGH_PRECISION).multiply(onePlusMonthlyRateToThePowerOfNumberOfPayments, HIGH_PRECISION);
        BigDecimal denominator = onePlusMonthlyRateToThePowerOfNumberOfPayments.subtract(ONE, HIGH_PRECISION);

        return nominator.divide(denominator, HIGH_PRECISION);
    }
}
