package com.example.quote.service;

import com.example.quote.lenderSource.LenderSource;
import com.example.quote.model.Loan;
import com.example.quote.model.QuoteResult;

import java.math.BigDecimal;
import java.util.List;

import static com.example.quote.Constants.HIGH_PRECISION;
import static com.example.quote.Constants.NUMBER_OF_PAYMENTS;
import static java.math.BigDecimal.valueOf;

public class QuoteCalculator {

    private final LenderSource lenderSource;
    private final RateCalculator rateCalculator;
    private final LoansBuilder loansBuilder;
    private final RepaymentsCalculator repaymentsCalculator;

    public QuoteCalculator(LenderSource lenderSource,
            RateCalculator rateCalculator,
            LoansBuilder loansBuilder, RepaymentsCalculator repaymentsCalculator) {
        this.lenderSource = lenderSource;
        this.rateCalculator = rateCalculator;
        this.loansBuilder = loansBuilder;
        this.repaymentsCalculator = repaymentsCalculator;
    }

    public QuoteResult getQuote(BigDecimal targetLoanValue) {
        List<Loan> loans = loansBuilder.buildLoans(targetLoanValue, lenderSource.getAvailableLenders());
        BigDecimal rate = rateCalculator.weightedAverage(loans);
        BigDecimal monthlyRepayment = repaymentsCalculator.constantRepayment(targetLoanValue, rate, NUMBER_OF_PAYMENTS);
        BigDecimal totalRepayment = monthlyRepayment.multiply(valueOf(NUMBER_OF_PAYMENTS), HIGH_PRECISION);
        return new QuoteResult(targetLoanValue, rate, monthlyRepayment, totalRepayment);
    }

}
