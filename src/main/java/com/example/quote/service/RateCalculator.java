package com.example.quote.service;

import com.example.quote.model.Loan;

import java.math.BigDecimal;
import java.util.List;

import static com.example.quote.Constants.HIGH_PRECISION;
import static java.math.BigDecimal.ZERO;

public class RateCalculator {

    BigDecimal weightedAverage(List<Loan> loans) {
        if (loans == null || loans.isEmpty()) {
            throw new IllegalArgumentException("loans are necessary to calculate rate");
        }

        BigDecimal accumulatedValue = ZERO;
        BigDecimal accumulatedWeight = ZERO;
        for (Loan loan : loans) {
            accumulatedValue = accumulatedValue.add(loan.getRate().multiply(loan.getAmount(), HIGH_PRECISION), HIGH_PRECISION);
            accumulatedWeight = accumulatedWeight.add(loan.getAmount());
        }
        return accumulatedValue.divide(accumulatedWeight, HIGH_PRECISION);
    }

}
