package com.example.quote.service;

import com.example.quote.exception.InsufficientOffersException;
import com.example.quote.model.Lender;
import com.example.quote.model.Loan;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import static com.example.quote.Constants.HIGH_PRECISION;
import static java.math.BigDecimal.ZERO;

public class LoansBuilder {

    List<Loan> buildLoans(final BigDecimal targetLoanValue, final List<Lender> availableLenders) {

        Iterator<Lender> iterator = availableLenders.stream().sorted(Comparator.comparing(Lender::getRate)).iterator();

        BigDecimal neededAmount = targetLoanValue;
        List<Loan> loans = new ArrayList<>();

        while (neededAmount.compareTo(ZERO) > 0 && iterator.hasNext()) {
            Lender lender = iterator.next();
            BigDecimal loanValue = loanValue(neededAmount, lender.getAvailableAmount());
            loans.add(new Loan(lender.getRate(), loanValue));
            neededAmount = neededAmount.subtract(loanValue, HIGH_PRECISION);
        }
        if (neededAmount.compareTo(ZERO) > 0) {
            throw new InsufficientOffersException();
        }
        return loans;

    }

    private BigDecimal loanValue(final BigDecimal neededAmount, final BigDecimal availableAmount) {
        return (neededAmount.compareTo(availableAmount) < 0) ? neededAmount : availableAmount;
    }

}
