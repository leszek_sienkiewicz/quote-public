package com.example.quote.lenderSource;

import com.example.quote.exception.CsvException;
import com.example.quote.model.Lender;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static java.util.stream.StreamSupport.stream;
import static org.apache.commons.csv.CSVFormat.DEFAULT;

public class CsvLenderSource implements LenderSource {

    public static final String LENDER = "Lender";
    public static final String RATE = "Rate";
    public static final String AVAILABLE = "Available";

    private static final CSVFormat CSV_FORMAT = DEFAULT.withFirstRecordAsHeader().withHeader(LENDER, RATE, AVAILABLE);

    private final String location;

    public CsvLenderSource(String location) {
        this.location = location;
    }

    @Override
    public List<Lender> getAvailableLenders() {
        try (Reader reader = new FileReader(location)) {
            return stream(CSV_FORMAT.parse(reader).spliterator(), false)
                    .map(this::buildLender)
                    .collect(toList());
        } catch (FileNotFoundException e) {
            throw new CsvException("The file does not exist: " + location, e);
        } catch (IOException e) {
            throw new CsvException("Unable to read or parse the file: " + location, e);
        }
    }

    private Lender buildLender(CSVRecord record) {
        try {
            String name = record.get(LENDER);
            BigDecimal rate = new BigDecimal(record.get(RATE));
            BigDecimal available = new BigDecimal(record.get(AVAILABLE));
            return new Lender(name, rate, available);
        } catch (Exception e) {
            throw new CsvException("Unable to parse row: " + record, e);
        }
    }
}
