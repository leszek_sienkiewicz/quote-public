package com.example.quote.lenderSource;

import com.example.quote.model.Lender;

import java.util.List;

public interface LenderSource {

    /**
     * Returns a list of currently available {@link Lender}s
     *
     * @return a list of {@link Lender}s
     */
    List<Lender> getAvailableLenders();

}
