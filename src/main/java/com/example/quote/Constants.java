package com.example.quote;

import java.math.BigDecimal;
import java.math.MathContext;

import static java.math.BigDecimal.valueOf;
import static java.math.MathContext.DECIMAL128;

public class Constants {

    public static final BigDecimal MONTHS_IN_YEAR = valueOf(12);
    public static final int NUMBER_OF_PAYMENTS = 3 * 12;

    public static final MathContext HIGH_PRECISION = DECIMAL128;

}
