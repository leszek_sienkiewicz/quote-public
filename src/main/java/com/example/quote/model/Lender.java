package com.example.quote.model;

import java.math.BigDecimal;

import static com.example.quote.exception.Validator.requireNotNull;
import static com.example.quote.exception.Validator.requirePositive;
import static java.math.BigDecimal.valueOf;

public class Lender {

    private final String name;
    private final BigDecimal rate;
    private final BigDecimal availableAmount;

    public Lender(String name, BigDecimal rate, BigDecimal availableAmount) {
        this.name = requireNotNull(name);
        this.rate = requirePositive(rate);
        this.availableAmount = requirePositive(availableAmount);
    }

    public static Lender lender(String name, double rate, double availableAmount) {
        return new Lender(name, valueOf(rate), valueOf(availableAmount));
    }

    public String getName() {
        return name;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public BigDecimal getAvailableAmount() {
        return availableAmount;
    }
}
