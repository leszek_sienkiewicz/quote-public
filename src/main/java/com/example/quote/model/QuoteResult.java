package com.example.quote.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static java.math.BigDecimal.valueOf;

public class QuoteResult {

    private final BigDecimal requestedAmount;
    private final BigDecimal rate;
    private final BigDecimal monthlyRepayment;
    private final BigDecimal totalRepayment;

    public QuoteResult(BigDecimal requestedAmount,
            BigDecimal rate,
            BigDecimal monthlyRepayment,
            BigDecimal totalRepayment) {
        this.requestedAmount = requestedAmount;
        this.rate = rate;
        this.monthlyRepayment = monthlyRepayment;
        this.totalRepayment = totalRepayment;
    }

    public BigDecimal getRequestedAmount() {
        return requestedAmount;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public BigDecimal getMonthlyRepayment() {
        return monthlyRepayment;
    }

    public BigDecimal getTotalRepayment() {
        return totalRepayment;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("Requested amount: £").append(formatCurrency(requestedAmount)).append("\n")
                .append("Rate: ").append(formatRate(rate)).append("%\n")
                .append("Monthly repayment: £").append(formatCurrency(monthlyRepayment)).append("\n")
                .append("Total repayment: £").append(formatCurrency(totalRepayment))
                .toString();
    }

    private BigDecimal formatRate(BigDecimal in) {
        return in.multiply(valueOf(100)).setScale(1, RoundingMode.HALF_UP);
    }

    private BigDecimal formatCurrency(BigDecimal in) {
        return in.setScale(2, RoundingMode.HALF_UP);
    }
}
