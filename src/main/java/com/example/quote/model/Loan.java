package com.example.quote.model;

import java.math.BigDecimal;

import static com.example.quote.exception.Validator.requirePositive;
import static java.math.BigDecimal.valueOf;

public class Loan {

    private final BigDecimal rate;
    private final BigDecimal amount;

    public Loan(BigDecimal rate, BigDecimal amount) {
        this.rate = requirePositive(rate);
        this.amount = requirePositive(amount);
    }

    public static Loan loan(double rate, double amount) {
        return new Loan(valueOf(rate), valueOf(amount));
    }

    public BigDecimal getRate() {
        return rate;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
