package com.example.quote.service;

import org.junit.Test;

import java.math.BigDecimal;

import static java.math.BigDecimal.valueOf;
import static org.junit.Assert.assertEquals;

public class RepaymentsCalculatorTest {

    private RepaymentsCalculator repaymentsCalculator = new RepaymentsCalculator();

    @Test
    public void oneMonthOnePercentMonthly() {
        BigDecimal result = repaymentsCalculator.constantRepayment(valueOf(1000), valueOf(0.12), 1);
        assertEquals(0, valueOf(1010).compareTo(result));
    }

    /**
     * In case of monthly rate of 100% the formula for constant installment simplifies to:
     * I = P * (2^n / (2^n - 1))
     * where
     * I - constant installment value
     * P - principal
     * n - number of installment
     */
    @Test
    public void oneHundredPercentMonthly() {
        assertEquals(valueOf(2000), calculate(1000, 12, 1)); // P * 2
        assertEquals(valueOf(4000), calculate(3000, 12, 2)); // P * 4/3
        assertEquals(valueOf(8000), calculate(7000, 12, 3)); // P * 8/7
        assertEquals(valueOf(1600), calculate(1500, 12, 4)); // P * 16/15
        assertEquals(valueOf(3200), calculate(3100, 12, 5)); // P * 32/31
        assertEquals(valueOf(6400), calculate(6300, 12, 6)); // P * 64/63
        assertEquals(valueOf(1280), calculate(1270, 12, 7)); // P * 128/127
        assertEquals(valueOf(2560), calculate(2550, 12, 8)); // P * 256/255
        assertEquals(valueOf(5120), calculate(5110, 12, 9)); // P * 512/511
    }

    /**
     * In case of monthly rate of 50% the formula simplifies to:
     * I = P / [(3/2)^-1 + (3/2)^-2 + ... + (3/2)^-n]
     */
    @Test
    public void fiftyPercentMonthly() {
        assertEquals(valueOf(3000), calculate(2000, 6, 1)); // P * 3/2
        assertEquals(valueOf(9000), calculate(10000, 6, 2)); // P * 9/10
        assertEquals(valueOf(2700), calculate(3800, 6, 3)); // P * 27/38
    }

    private BigDecimal calculate(long principal, long rate, int numberOfPayments) {
        return repaymentsCalculator.constantRepayment(valueOf(principal), valueOf(rate), numberOfPayments).setScale(0);
    }

}
