package com.example.quote.service;

import com.example.quote.lenderSource.LenderSource;
import com.example.quote.model.QuoteResult;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static java.math.BigDecimal.valueOf;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

public class QuoteCalculatorTest {

    private QuoteCalculator calculator;
    private LenderSource lenderSourceMock;
    private RateCalculator rateCalculatorMock;
    private LoansBuilder loansBuilderMock;
    private RepaymentsCalculator repaymentsCalculator;

    @Before
    public void beforeEachTest() {
        lenderSourceMock = Mockito.mock(LenderSource.class);
        rateCalculatorMock = Mockito.mock(RateCalculator.class);
        loansBuilderMock = Mockito.mock(LoansBuilder.class);
        repaymentsCalculator = Mockito.mock(RepaymentsCalculator.class);
        calculator = new QuoteCalculator(lenderSourceMock, rateCalculatorMock, loansBuilderMock, repaymentsCalculator);
    }

    @Test
    public void calculatorShouldReturnResultBasedOnRateCalculation() {
        when(rateCalculatorMock.weightedAverage(any())).thenReturn(valueOf(0.05));
        when(repaymentsCalculator.constantRepayment(any(), any(), anyInt())).thenReturn(valueOf(200));
        QuoteResult quoteResult = calculator.getQuote(valueOf(5000));
        assertEquals(valueOf(5000), quoteResult.getRequestedAmount());
        assertEquals(valueOf(0.05), quoteResult.getRate());
        assertEquals(valueOf(200), quoteResult.getMonthlyRepayment());
        assertEquals(valueOf(7200), quoteResult.getTotalRepayment());
    }

}
