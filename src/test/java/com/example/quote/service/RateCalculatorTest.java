package com.example.quote.service;

import com.example.quote.model.Loan;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

import static com.example.quote.model.Loan.loan;
import static java.math.BigDecimal.valueOf;
import static java.math.RoundingMode.HALF_UP;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;

public class RateCalculatorTest {

    private RateCalculator rateCalculator = new RateCalculator();

    @Test(expected = IllegalArgumentException.class)
    public void nullCausesException() {
        rateCalculator.weightedAverage(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void emptyListCausesException() {
        rateCalculator.weightedAverage(loans());
    }

    @Test
    public void oneLoan() {
        BigDecimal rate = rateCalculator.weightedAverage(loans(loan(0.07, 1000)));
        assertEquals(valueOf(0.07), rate);
    }

    @Test
    public void twoEqualLoans() {
        List<Loan> loans = loans(loan(0.07, 1000), loan(0.07, 1000));
        BigDecimal rate = rateCalculator.weightedAverage(loans);
        assertEquals(valueOf(0.07), rate);
    }

    @Test
    public void averageOfTwoLoans() {
        List<Loan> loans = loans(loan(0.05, 1000), loan(0.1, 1000));
        BigDecimal rate = rateCalculator.weightedAverage(loans);
        assertEquals(valueOf(0.075), rate);
    }

    @Test
    public void weightedAverageOfTwoLoans() {
        List<Loan> loans = loans(loan(0.01, 6000), loan(0.02, 4000));
        BigDecimal rate = rateCalculator.weightedAverage(loans);
        assertEquals(valueOf(0.014), rate);
    }

    @Test
    public void tenDividedByThree() {
        List<Loan> loans = loans(loan(0.05, 1000), loan(0.025, 2000));
        BigDecimal rate = rateCalculator.weightedAverage(loans);
        assertEquals(valueOf(0.033333), rate.setScale(6, HALF_UP));
    }

    private List<Loan> loans(Loan... loans) {
        return stream(loans).collect(toList());
    }

}
