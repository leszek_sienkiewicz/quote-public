package com.example.quote.service;

import com.example.quote.exception.InsufficientOffersException;
import com.example.quote.model.Lender;
import com.example.quote.model.Loan;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

import static com.example.quote.model.Lender.lender;
import static java.math.BigDecimal.ROUND_UNNECESSARY;
import static java.math.BigDecimal.ZERO;
import static java.math.BigDecimal.valueOf;
import static java.math.MathContext.UNLIMITED;
import static java.util.Arrays.stream;
import static java.util.UUID.randomUUID;
import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class LoansBuilderTest {

    private static final Lender LENDER_7_PERCENT_2000_POUNDS = lender(randomUUID().toString(), 0.07, 2000);
    private static final Lender LENDER_8_PERCENT_4000_POUNDS = lender(randomUUID().toString(), 0.08, 4000);

    private LoansBuilder loansBuilder = new LoansBuilder();

    @Test(expected = InsufficientOffersException.class)
    public void emptyListOfLendersShouldResultInException() {
        loansBuilder.buildLoans(valueOf(2100), lenders());
    }

    @Test(expected = InsufficientOffersException.class)
    public void insufficientOffersShouldResultInException() {
        loansBuilder.buildLoans(valueOf(2100), lenders(LENDER_7_PERCENT_2000_POUNDS));
    }

    @Test
    public void oneLender() {
        List<Loan> loans = loansBuilder.buildLoans(valueOf(1000), lenders(LENDER_7_PERCENT_2000_POUNDS));
        assertEquals(1, loans.size());
        assertEquals(valueOf(1000), sumOfLoanAmounts(loans));
    }

    @Test
    public void fractionOfOfferedAmountShouldBeAccepted() {
        List<Lender> lenders = lenders(LENDER_7_PERCENT_2000_POUNDS, LENDER_8_PERCENT_4000_POUNDS);
        List<Loan> loans = loansBuilder.buildLoans(valueOf(3000), lenders);
        assertEquals(2, loans.size());
        assertEquals(valueOf(3000), sumOfLoanAmounts(loans));
        assertListContains(loans, valueOf(0.07), valueOf(2000));
        assertListContains(loans, valueOf(0.08), valueOf(1000));
    }

    @Test
    public void lendersShouldBeSortedByRate() {
        List<Lender> lenders = lenders(LENDER_8_PERCENT_4000_POUNDS, LENDER_7_PERCENT_2000_POUNDS);
        List<Loan> loans = loansBuilder.buildLoans(valueOf(5000), lenders);
        assertEquals(2, loans.size());
        assertEquals(valueOf(5000), sumOfLoanAmounts(loans));
        assertListContains(loans, valueOf(0.07), valueOf(2000));
        assertListContains(loans, valueOf(0.08), valueOf(3000));
    }

    private void assertListContains(List<Loan> loans, BigDecimal rate, BigDecimal amount) {
        assertTrue(loans.stream()
                .filter($ -> $.getRate().compareTo(rate) == 0)
                .filter($ -> $.getAmount().compareTo(amount) == 0)
                .count() > 0);
    }

    private BigDecimal sumOfLoanAmounts(List<Loan> loans) {
        return loans.stream()
                .map(Loan::getAmount)
                .reduce(ZERO, (a, b) -> a.add(b, UNLIMITED))
                .setScale(0);
    }

    private List<Lender> lenders(Lender ... lenders) {
        return stream(lenders).collect(toList());
    }

}
