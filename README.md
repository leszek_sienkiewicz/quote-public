# Quote

A simple, command line application for calculating a quote for potential borrowers in a peer-to-peer lending platform.

## How to build

    mvn clean install

The above command builds a fat jar (using the mvn shade plugin), which can later be run as a standalone command line program.

## How to run

    java -jar [path/to/the/fat/jar] [path/to/the/input/file] [requested-amount]

There is a bash script to conveniently run the application directly after building:

    run.sh [path/to/the/input/file] [requested-amount]


## Notes, assumptions and decisions:

* The loan is always given for a period of 3 years, each year consisting of 12 moths of equal length
* Each loan is divided among the lenders in proportion to the offered amount in the target amount and each repayment is done each month to all lenders in the same proportion
* The overall interest rate for the whole loan is calculated as the weighted average of the rates of partial loans (in proportion of each partial loan in the sum)
* The monthly payment amount is calculated using "Monthly amortized loan" method, which is the loan equivalent of monthly compounding interest in deposits (see https://en.wikipedia.org/wiki/Mortgage_calculator#Monthly_payment_formula)
* The intermediate calculations use high precision when rounding is necessary, to minimize the accumulating error (MathContext.DECIMAL128)
* The rounding for presentation is performed using ROUND_HALF_UP method
